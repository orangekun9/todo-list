import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.*;
import java.awt.Font;


public class mainFrame {

	private JFrame frmTodoList;
	private JTable table;
	private JTextField txtTask;
	// Initializing objects and variables for later use
	private boolean sortDesc = true;
	private HashMap<Integer, TODO> map;
	private ArrayList<TODO> todo_list;
	private JTextField txtSearch;
	private Task_Inteface task = new Task();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					mainFrame window = new mainFrame();
					window.frmTodoList.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	//  This function is responsible for filling the table
	private void loadValues() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		map = task.getMap();
		
		// Created ArrayList, this ArrayList is used later for the row specific manipulation which are update and delete
		todo_list = new ArrayList<TODO>();
		
		// Created HashMap, this HashMap is responsible for maintaining priority with respect to their priority index
		HashMap<String, Integer> priority_map = new HashMap<String, Integer>();
		priority_map.put("Critical", 1);
		priority_map.put("Major", 2);
		priority_map.put("Minor/Moderate", 3);
		priority_map.put("Low", 4);
		
		// This code if for clearing the current filled values on JTable
		if(model.getRowCount() > 0) {
			for(int i=model.getRowCount() - 1; i > -1; i--) {
				model.removeRow(i);
			}
		}
		
		// Created Priority Queue, this Priority queue is responsible for maintaining the tasks(TODO class) according to their priority
		PriorityQueue<Pair<Integer, TODO>> queue;
		
		// Implementing both types of Priority Queue (max heap and min heap), created a new comparator for both min and max heaps
		// Created and used my own datatype (Pair class) two hold two types of different values, Pair class is used while priority queue is using its comparator
		if(sortDesc) {
			queue = new PriorityQueue<Pair<Integer, TODO>>(new The_Comperator_Desc());
		}
		else {
			queue = new PriorityQueue<Pair<Integer, TODO>>(new The_Comperator_Asc());
		}
		
		Iterable<Integer> Keys = map.keySet();
		
		// Adding values in Priority queue
		for(int k: Keys) {
				Pair<Integer, TODO> p = new Pair<Integer, TODO>();
				p.setFirst(priority_map.get(map.get(k).getPriority()));
				p.setSecond(map.get(k));
				queue.add(p);
		}
		
		// Filling values in the JTable and inserting values in ArrayList for later manipulation tasks
		while(!queue.isEmpty()) {
			Pair<Integer, TODO> data = queue.poll();
			model.addRow(new Object[] {data.getSecond().getTask(), data.getSecond().getPriority()});
			todo_list.add(data.getSecond());
		}
		
	}
	
	
	// This function is responsible for changing the color of the particular and all rows in JTable
	private void setRowColour(int row) {
		table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
			@Override
		    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int ro, int column)
		    {
		        final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		        if(row != -1) {
		        	c.setBackground(ro - row == 0 ? Color.GREEN : Color.WHITE);
		        }
		        else {
		        	c.setBackground(Color.WHITE);
		        }
		        
		        return c;
		    }
		});
	}
	
	

	/**
	 * Create the application.
	 */
	public mainFrame() {
		initialize();
		frmTodoList.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTodoList = new JFrame();
		frmTodoList.setTitle("TODO List");
		frmTodoList.setBounds(100, 100, 450, 608);
		frmTodoList.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTodoList.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 414, 334);
		frmTodoList.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 414, 334);
		panel.add(scrollPane);
		table = new JTable(new DefaultTableModel(new Object[][] {}, new String[] {"Task", "Priority"}));
		scrollPane.setViewportView(table);
		
		txtTask = new JTextField();
		txtTask.setBounds(135, 356, 239, 20);
		frmTodoList.getContentPane().add(txtTask);
		txtTask.setColumns(10);
		
		JLabel lblTask = new JLabel("Task: ");
		lblTask.setFont(new Font("SansSerif", Font.PLAIN, 13));
		lblTask.setBounds(51, 356, 46, 20);
		frmTodoList.getContentPane().add(lblTask);
		
		JLabel lblPriority = new JLabel("Priority: ");
		lblPriority.setFont(new Font("SansSerif", Font.PLAIN, 13));
		lblPriority.setBounds(52, 406, 56, 17);
		frmTodoList.getContentPane().add(lblPriority);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"", "Critical", "Major", "Minor/Moderate", "Low"}));
		comboBox.setBounds(135, 403, 239, 20);
		frmTodoList.getContentPane().add(comboBox);
		
		
		
		JButton btnAdd = new JButton("Add");
		btnAdd.setFont(new Font("SansSerif", Font.PLAIN, 11));
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setRowColour(-1);
				if(txtTask.getText().equals("") || comboBox.getSelectedItem().toString().equals("")) {
					JOptionPane.showMessageDialog(frmTodoList, "Please complete all the fields");
				}
				else {
					TODO t = new TODO();
					t.setTask(txtTask.getText());
					t.setPriority(comboBox.getSelectedItem().toString());
			
					// Getting the saved map from system (Implemented on Task class)
					map = task.getMap();
					
					Iterable<Integer> Keys =   map.keySet();
					
					//Getting current max id
					int id=0;
					for(int k: Keys) {
						id = Math.max(id, k);
					}
					
					t.setId(++id);
					map.put(id, t);
					
					// Saving the updated map on the system
					task.saveMap(map);
					
					txtTask.setText("");
					comboBox.setSelectedItem("");
					
					// Loading the JTable
					loadValues();
				}
			}
		});
		btnAdd.setBounds(51, 454, 147, 23);
		frmTodoList.getContentPane().add(btnAdd);
		
		JButton btnSort = new JButton("Sort Priorities \u25B2\u25BC");
		btnSort.setFont(new Font("SansSerif", Font.PLAIN, 11));
		btnSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Changing heap type for Priority Queue
				sortDesc = !sortDesc;
				setRowColour(-1);
				loadValues();
			}
		});
		btnSort.setBounds(227, 454, 147, 23);
		frmTodoList.getContentPane().add(btnSort);
		
		// Searching by task listener
		JButton btnSearch = new JButton("Search");
		btnSearch.setFont(new Font("SansSerif", Font.PLAIN, 11));
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtSearch.getText().equals("") || txtSearch.getText().equals(" ")) {
					JOptionPane.showMessageDialog(frmTodoList, "Please enter valid input to search");
					setRowColour(-1);
					loadValues();
				}
				else {
					// Setting the row color according to serach result
					String task_val = txtSearch.getText();
					int row = -1;
					for(int i=0;i<todo_list.size();i++) {
						if(todo_list.get(i).getTask().equals(task_val)) {
							row = i;
							setRowColour(row);
							break;
						}
					}
					
					if(row == -1) {
						JOptionPane.showMessageDialog(frmTodoList, "Error 404: Task not found");
						setRowColour(-1);
					}
					txtSearch.setText("");
					loadValues();
				}
			}
		});
		btnSearch.setBounds(51, 508, 89, 23);
		frmTodoList.getContentPane().add(btnSearch);
		
		txtSearch = new JTextField();
		txtSearch.setBounds(150, 510, 180, 21);
		frmTodoList.getContentPane().add(txtSearch);
		txtSearch.setColumns(10);
		
		JButton btnX = new JButton("X");
		btnX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setRowColour(-1);
				loadValues();
			}
		});
		btnX.setFont(new Font("SansSerif", Font.BOLD, 11));
		btnX.setBounds(328, 509, 46, 20);
		frmTodoList.getContentPane().add(btnX);
		
		
		// Row click action listener, for showing update and delete option tab and performing those operations
		ListSelectionModel model = table.getSelectionModel();
		model.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				if(!model.isSelectionEmpty()) {
					
					Object[] options = {"Update", "Delete"};
					// Get Selected Row
					int selectedRow = model.getMinSelectionIndex();
					try {
						
						String result = (String)JOptionPane.showInputDialog(null, "Select your action", "Actions", JOptionPane.YES_NO_CANCEL_OPTION, null, options, null);
						if(result.equals("Update")) {
							new updateFrame(todo_list.get(selectedRow));
							frmTodoList.dispose();
						}
						else if(result.equals("Delete")) {
							map = task.getMap();
							map.remove(todo_list.get(selectedRow).getId());
							task.saveMap(map);
						}
					}catch(Exception ex) {
						// Nothing was selected by user
					}
					setRowColour(-1);
					loadValues();
				}
			}
		});
		
		
		loadValues();
	}
}
