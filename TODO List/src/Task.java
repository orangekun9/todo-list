import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;


public class Task implements Task_Inteface{
	
	public HashMap<Integer, TODO> getMap(){			
		HashMap<Integer, TODO> map = new HashMap<Integer, TODO>();
		try{
			ObjectInputStream in=new ObjectInputStream(new FileInputStream("TODO_data.dat"));
			map = (HashMap<Integer, TODO>)in.readObject();
			in.close();
		}
		catch(Exception e2){}
		
		return map;
	}
	
	public void saveMap(HashMap<Integer, TODO> map) {
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("TODO_data.dat"));
			out.writeObject(map);
			out.close();
		}
		catch(Exception e2) {}
	}
}
