import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class updateFrame {

	private JFrame frmTodoList;
	private JTextField txtTask;
	private TODO data;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					updateFrame window = new updateFrame();
					window.frmTodoList.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public updateFrame() {
		initialize();
		frmTodoList.setVisible(true);
	}
	
	public updateFrame(TODO t) {
		data = t;
		initialize();
		frmTodoList.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTodoList = new JFrame();
		frmTodoList.setTitle("TODO List");
		frmTodoList.setBounds(100, 100, 438, 407);
		frmTodoList.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTodoList.getContentPane().setLayout(null);
		
		JLabel lblUpdateTask = new JLabel("Update Task");
		lblUpdateTask.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblUpdateTask.setBounds(162, 27, 110, 23);
		frmTodoList.getContentPane().add(lblUpdateTask);
		
		JLabel lblTask = new JLabel("Task: ");
		lblTask.setBounds(64, 140, 46, 14);
		frmTodoList.getContentPane().add(lblTask);
		
		JLabel lblPriority = new JLabel("Priority: ");
		lblPriority.setBounds(64, 198, 58, 14);
		frmTodoList.getContentPane().add(lblPriority);
		
		txtTask = new JTextField();
		txtTask.setBounds(120, 137, 218, 20);
		frmTodoList.getContentPane().add(txtTask);
		txtTask.setColumns(10);
		txtTask.setText(data.getTask());
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"", "Critical", "Major", "Minor/Moderate", "Low"}));
		comboBox.setBounds(120, 195, 218, 20);
		frmTodoList.getContentPane().add(comboBox);
		comboBox.setSelectedItem(data.getPriority());
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.setFont(new Font("SansSerif", Font.PLAIN, 11));
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtTask.getText().equals("") || txtTask.getText().equals(" ") || comboBox.getSelectedItem().toString().equals("")) {
					JOptionPane.showMessageDialog(frmTodoList, "Please complete all the fields");
				}
				else {
					String task = txtTask.getText();
					String priority = comboBox.getSelectedItem().toString();
					data.setTask(task);
					data.setPriority(priority);
					
					Task t = new Task();
					HashMap<Integer, TODO> map = t.getMap();
					map.put(data.getId(), data);
					t.saveMap(map);
					frmTodoList.dispose();
					new mainFrame();
				}
				
			}
		});
		btnUpdate.setBounds(162, 285, 89, 23);
		frmTodoList.getContentPane().add(btnUpdate);
		
		JButton btnBack = new JButton("Back");
		btnBack.setFont(new Font("SansSerif", Font.PLAIN, 11));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmTodoList.dispose();
				new mainFrame();
			}
		});
		btnBack.setBounds(333, 0, 89, 23);
		frmTodoList.getContentPane().add(btnBack);
	}
}
